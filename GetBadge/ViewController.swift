//
//  ViewController.swift
//  GetBadge
//
//  Created by NSmych on 24.09.2020.
//  Copyright © 2020 NSmych. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var button: UIButton!
    let myView = UIView()
    let currentDate = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
    let newbie = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 30))
    let textB = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 80))
    let pic = UIImage(named: "newbie.png")
    let myImageView:UIImageView = UIImageView()
    let myColor = UIColor.init(red: 249/255, green: 195/255, blue: 220/255, alpha: 1)
    
    @IBAction func getBadge(_ sender: UIButton) {
        button.alpha = 0.0
        myView.backgroundColor = .white
        
        // CURRENT DATE
        let currentDateX = self.view.frame.width / 2 - currentDate.frame.width / 2
        let currentDateY = self.view.frame.height / 5.2
        currentDate.frame.origin = CGPoint(x: currentDateX, y: currentDateY)
        currentDate.font = UIFont(name: "Avenir Book", size: 28)
        currentDate.textAlignment = .center
        
        // NEWBIE
        let newbieX = self.view.frame.width / 2 - newbie.frame.width / 2
        let newbieY = self.view.frame.height / 3.8 - myImageView.frame.height / 2
        newbie.frame.origin = CGPoint(x: newbieX, y: newbieY)
        newbie.font = UIFont(name: "Avenir Heavy", size: 34.0)
        newbie.text = "Newbie"
        newbie.textAlignment = .center
        
        // PICTURE
        myImageView.contentMode = UIView.ContentMode.scaleAspectFit
        myImageView.frame.size.width = 230
        myImageView.frame.size.height = 360
        myImageView.center = self.view.center
        myImageView.image = pic
        
        // BADGE TEXT
        let textBX = self.view.frame.width / 2 - textB.frame.width / 2
        let textBY = self.view.frame.height / 2 + myImageView.frame.height / 2.9
        textB.frame.origin = CGPoint(x: textBX, y: textBY)
        textB.font = UIFont(name: "Avenir Next Condensed Regular", size: 24)
        textB.lineBreakMode = .byWordWrapping
        textB.numberOfLines = 0
        textB.textAlignment = .center
        textB.text = "Added the first flight. Get 1 notification subscription for free!"
        
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let date = Date()
        currentDate.text = dateFormatter.string(from: date)

        view.addSubview(currentDate)
        view.addSubview(newbie)
        view.addSubview(myImageView)
        view.addSubview(textB)

        view.backgroundColor = .white
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        guard let badge = UIGraphicsGetImageFromCurrentImageContext()
            else { return }
        let activityVC = UIActivityViewController(activityItems: [badge, "Enjoy your badge! Save it :)"], applicationActivities: nil)
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityVC, animated: true, completion: nil)
        getAllBack()
    }
    
    private func getAllBack() {
        button.alpha = 1.0
        view.backgroundColor = myColor
        currentDate.alpha = 0.0
        newbie.alpha = 0.0
        myImageView.alpha = 0.0
        textB.alpha = 0.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = myColor
        button.backgroundColor = UIColor.init(red: 106/255, green: 16/255, blue: 100/255, alpha: 1)
        button.layer.cornerRadius = 25.0
        button.tintColor = .white
    }
}

